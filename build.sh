#!/bin/sh
set -eux
cargo fmt
cargo check --all
cargo clippy
cargo test
cargo build --release
