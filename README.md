# rh_server

Software to distribute video files to a web server to create a shared streaming experience

## Installation

1. Install Rust Stable >=1.77.2
1. Run `./build.sh`
1. Run the server: `cargo run`
