mod app_state;
mod server;

use std::{fs, time::Duration};

use actix_rt::time;
use actix_web::{web, App, HttpServer};
use app_state::VideoFile;
use server::api;

use crate::app_state::Updater;

const TIME_RATE: Duration = Duration::from_secs(1);
const DATA_FILE_NAME: &str = "data.json";

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let file_content = fs::read_to_string(DATA_FILE_NAME).expect("Unable to read video file");
    let json_content: Vec<VideoFile> = serde_json::from_str(&file_content)?;
    let mut updater = Updater::new(json_content);
    let app_state = updater.app_state.clone();

    actix_rt::spawn(async move {
        let mut interval = time::interval(TIME_RATE);
        loop {
            interval.tick().await;
            updater.update_state();
        }
    });

    HttpServer::new(move || {
        let data = web::Data::new(app_state.clone());
        App::new()
            .app_data(web::Data::clone(&data))
            .route("/api/v1", web::get().to(api))
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}
