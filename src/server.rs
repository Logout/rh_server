use actix_web::{web, Responder};

use crate::app_state::AppState;

pub async fn api(app_state_wrapper: web::Data<AppState>) -> impl Responder {
    let app_state_pointer = app_state_wrapper.playback_info.lock().unwrap();
    web::Json(app_state_pointer.clone())
}

#[cfg(test)]
mod tests {
    use std::{
        sync::{Arc, Mutex},
        time::Duration,
    };

    use crate::app_state::{AppState, PlaybackInfo};

    use super::*;
    use actix_web::{test, App};

    #[actix_web::test]
    async fn test_api_return_struct() {
        let app_state = web::Data::new(AppState {
            playback_info: Arc::new(Mutex::new(PlaybackInfo {
                current_file: "1".to_string(),
                current_title: "5".to_string(),
                duration: Duration::from_secs(2),
                current_time_in_s: Duration::from_secs(3),
                next_file: "4".to_string(),
            })),
        });
        let app = test::init_service(
            App::new()
                .app_data(web::Data::clone(&app_state))
                .route("/api/v1/", web::get().to(api)),
        )
        .await;
        let req = test::TestRequest::get().uri("/api/v1/").to_request();
        let resp: PlaybackInfo = test::call_and_read_body_json(&app, req).await;

        assert_eq!(resp.current_file, "1");
        assert_eq!(resp.current_title, "5");
        assert_eq!(resp.duration, Duration::from_secs(2));
        assert_eq!(resp.current_time_in_s, Duration::from_secs(3));
        assert_eq!(resp.next_file, "4");
    }
}
