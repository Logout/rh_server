extern crate rand;

use serde::{Deserialize, Serialize};
use serde_with::{serde_as, DurationSeconds};
use std::mem;
use std::sync::{Arc, Mutex};
use std::time::Duration;

#[cfg(not(test))]
use rand::{seq::SliceRandom, thread_rng};

use crate::TIME_RATE;

#[serde_as]
#[derive(Clone, Serialize, Deserialize, Default)]
pub(crate) struct PlaybackInfo {
    pub current_file: String,
    pub current_title: String,
    #[serde_as(as = "DurationSeconds<u64>")]
    pub duration: Duration,
    #[serde_as(as = "DurationSeconds<u64>")]
    pub current_time_in_s: Duration,
    pub next_file: String,
}

impl PlaybackInfo {
    fn increase_time_stamp(&mut self) {
        self.current_time_in_s += TIME_RATE;
    }

    fn update_video_file(&mut self, current_video_file: &VideoFile, next_video_file: &VideoFile) {
        self.current_file.clone_from(&current_video_file.file_name);
        self.current_title.clone_from(&current_video_file.title);
        self.duration = current_video_file.duration;
        self.next_file.clone_from(&next_video_file.file_name);
    }
}

#[serde_as]
#[derive(Default, Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct VideoFile {
    pub file_name: String,
    pub title: String,
    #[serde_as(as = "DurationSeconds<u64>")]
    pub duration: Duration,
}

struct VideoFiles {
    video_files: Vec<VideoFile>,
    next_element_index: usize,
}

impl VideoFiles {
    fn new(video_file_list: Vec<VideoFile>) -> VideoFiles {
        VideoFiles {
            video_files: video_file_list,
            next_element_index: 2,
        }
    }
}

#[cfg(not(test))]
fn shuffle_vec<T>(target_vec: &mut Vec<T>) {
    let mut rng = thread_rng();
    let slice: &mut [T] = target_vec;
    slice.shuffle(&mut rng);
}

impl Iterator for VideoFiles {
    type Item = VideoFile;

    fn next(&mut self) -> Option<Self::Item> {
        match self.video_files.get(self.next_element_index) {
            Some(video_file) => {
                self.next_element_index += 1;
                Some(video_file.to_owned())
            }
            None => {
                if self.video_files.len() < 2 {
                    return None;
                }
                shuffle_vec(&mut self.video_files);
                self.next_element_index = 0;
                self.next()
            }
        }
    }
}

#[derive(Default, Clone)]
pub(crate) struct AppState {
    pub(crate) playback_info: Arc<Mutex<PlaybackInfo>>,
}

pub struct Updater {
    pub app_state: AppState,
    video_files: VideoFiles,
    current_video_file: VideoFile,
    next_video_file: VideoFile,
}

impl Updater {
    pub fn new(video_file_list: Vec<VideoFile>) -> Updater {
        let app_state = AppState::default();
        let current_video_file = video_file_list[0].clone();
        let next_video_file = video_file_list[1].clone();
        let video_files = VideoFiles::new(video_file_list);
        app_state
            .playback_info
            .lock()
            .unwrap()
            .update_video_file(&current_video_file, &next_video_file);
        Updater {
            app_state,
            video_files,
            current_video_file,
            next_video_file,
        }
    }

    pub fn update_state(&mut self) {
        let mut app_state = self.app_state.playback_info.lock().unwrap();
        app_state.increase_time_stamp();
        if app_state.current_time_in_s >= app_state.duration {
            app_state.current_time_in_s = Duration::from_secs(0);
            if let Some(next_video_file) = self.video_files.next() {
                self.current_video_file = mem::replace(&mut self.next_video_file, next_video_file);
                app_state.update_video_file(&self.current_video_file, &self.next_video_file);
            }
        }
    }
}

#[cfg(test)]
fn shuffle_vec<T>(target_vec: &mut Vec<T>) {
    target_vec.reverse();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn iterator_supports_empty_list() {
        let mut empty_files = VideoFiles::new(vec![]);
        assert_eq!(None, empty_files.next());
    }

    #[test]
    fn shuffles_video_file_list() {
        let mut updater = Updater::new(create_minimal_video_list());
        assert_eq!(get_playback_info(&updater).current_file, "1");
        assert_eq!(get_playback_info(&updater).current_title, "2");
        assert_eq!(get_playback_info(&updater).next_file, "3");
        updater.update_state();
        updater.update_state();
        updater.update_state();
        assert_eq!(get_playback_info(&updater).current_file, "3");
        assert_eq!(get_playback_info(&updater).next_file, "3");
        updater.update_state();
        assert_eq!(get_playback_info(&updater).current_file, "3");
        assert_eq!(get_playback_info(&updater).next_file, "1");
    }

    #[test]
    fn current_time_should_monotonically_increase() {
        let mut updater = Updater::new(create_minimal_video_list());
        updater.update_state();
        assert_eq!(
            get_playback_info(&updater).current_time_in_s,
            Duration::from_secs(1)
        );
        updater.update_state();
        assert_eq!(
            get_playback_info(&updater).current_time_in_s,
            Duration::from_secs(2)
        );
    }

    #[test]
    fn should_switch_video_files() {
        let video_files = vec![
            VideoFile {
                file_name: "1".to_string(),
                title: "2".to_string(),
                duration: Duration::from_secs(2),
            },
            VideoFile {
                file_name: "3".to_string(),
                title: "4".to_string(),
                duration: Duration::from_secs(1),
            },
            VideoFile {
                file_name: "5".to_string(),
                title: "6".to_string(),
                duration: Duration::from_secs(1),
            },
            VideoFile {
                file_name: "7".to_string(),
                title: "8".to_string(),
                duration: Duration::from_secs(1),
            },
        ];
        let mut updater = Updater::new(video_files);
        assert_eq!(
            get_playback_info(&updater).current_time_in_s,
            Duration::from_secs(0)
        );
        assert_eq!(get_playback_info(&updater).duration, Duration::from_secs(2));
        assert_eq!(get_playback_info(&updater).current_file, "1");
        assert_eq!(get_playback_info(&updater).next_file, "3");
        updater.update_state();
        assert_eq!(get_playback_info(&updater).current_file, "1");
        assert_eq!(
            get_playback_info(&updater).current_time_in_s,
            Duration::from_secs(1)
        );
        updater.update_state();
        assert_eq!(get_playback_info(&updater).current_file, "3");
        assert_eq!(
            get_playback_info(&updater).current_time_in_s,
            Duration::from_secs(0)
        );
        assert_eq!(get_playback_info(&updater).next_file, "5");
        updater.update_state();
        assert_eq!(get_playback_info(&updater).current_file, "5");
        assert_eq!(get_playback_info(&updater).next_file, "7");
    }

    fn get_playback_info(updater: &Updater) -> PlaybackInfo {
        updater.app_state.playback_info.lock().unwrap().clone()
    }

    fn create_minimal_video_list() -> Vec<VideoFile> {
        vec![
            VideoFile {
                file_name: "1".to_string(),
                title: "2".to_string(),
                duration: Duration::from_secs(3),
            },
            VideoFile {
                file_name: "3".to_string(),
                title: "4".to_string(),
                duration: Duration::from_secs(1),
            },
        ]
    }
}
